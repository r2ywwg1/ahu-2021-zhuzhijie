from calculator import *
text = input("Please input the formula that you want to calculate:")    #提示用户输入
operator_list = '+-*/'  #操作符列表（可扩充）
pos = -1    #定位操作符的位置
for i in range(0, 5):   #寻找操作符的位置
    pos = text.find(operator_list[i])
    if pos != -1:
        break
processed_text = text.partition(text[pos])  #对初始文本进行切片分隔，返回一个元组
a = calculator(int(processed_text[0]))  #实例化一个初始类，以左值作为该类的值
a.operate(processed_text[1], int(processed_text[2]))    #进行操作，并输出结果
