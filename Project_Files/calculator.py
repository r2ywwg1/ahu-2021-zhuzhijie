class  calculator(object):  #设计一个计算器对象类
    count = 0
    def __init__(self, value):  #类的初始化方法
        self.num = value    #定义计算器对象的值
        calculator.count += 1   #用于跟踪存在多少个calculator实例
    def __del__(self):
        calculator.count -= 1   #类的销毁方法
    def operate(self, operator, rvalue):    #类的操作，operator参数是操作符，rvalue是待操作的右值
        if operator == '+':
            self.num += rvalue
        elif operator == '-':
            self.num -= rvalue
        elif operator == '*':
            self.num *= rvalue
        else:
            self.num /= rvalue
        print(self.num) #输出对象的值